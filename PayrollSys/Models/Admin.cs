﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class Admin
    {
        [Required]
        public int id { get; set; }

        [Required]
        [DisplayName("Admin Full Name")]
        public string fullName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }
        
        [Required]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Phone Number")]
        public string PhoneNo { get; set; }

        [Required]
        public string RoleName { get; set; }
    }
}
