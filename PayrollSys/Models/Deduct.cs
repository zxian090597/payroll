﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class Deduct
    {
        [Required]
        public int DeductId { get; set; }

        [Required]
        public string criteria { get; set; }

        [Required]
        public double rate { get; set; }

        public string note { get; set; }

        public List<dvalue> dvalue { get; set; }
    }
}
