﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class Advance
    {
        [Required]
        public int id { get; set; }

        [DisplayName("Employee")]
        public int employeeID { get; set; }

        [DisplayName("Year-Month")]
        [Required(ErrorMessage = "Please enter month")]
        public string ym { get; set; }

        [DisplayName("Status")]
        [Required]
        public string status { get; set; }

        public string employeeName { get; set; }
    }
}
