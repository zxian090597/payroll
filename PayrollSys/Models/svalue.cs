﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class svalue
    {
        [Required]
        public int salaryId { get; set; }

        public Socso socso { get; set; }

        [Required]
        public int socsoId { get; set; }

        public Salary Salary { get; set; }

        [Required]
        public string ym { get; set; }

        [Required]
        public double employee { get; set; }

        [Required]
        public double employer { get; set; }

    }
}
