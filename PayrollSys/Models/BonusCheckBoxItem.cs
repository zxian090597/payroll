﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PayrollSys.Models
{
    public class BonusCheckBoxItem
    {
        public int BonusId { get; set; }

        public string criteria { get; set; }

        public double rate { get; set; }

        public bool IsChecked { get; set; }
    }
}