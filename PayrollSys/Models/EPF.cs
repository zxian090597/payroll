﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class EPF
    {
        [Required]
        public int EPFId { get; set; }

        [Required]
        public double Employee { get; set; }

        [Required]
        public double Employer { get; set; }

        public List<evalue> evalue { get; set; }
    }
}
