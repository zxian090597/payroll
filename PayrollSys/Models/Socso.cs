﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class Socso
    {
        [Required]
        public int SocsoId { get; set; }

        [Required]
        public double Pay_From { get; set; }

        [Required]
        public double Pay_To { get; set; }

        [Required]
        public double Employee { get; set; }

        [Required]
        public double Employer { get; set; }

        public List<svalue> svalue { get; set; }

    }
}
