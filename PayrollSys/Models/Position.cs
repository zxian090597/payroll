﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class Position
    {
        [Required]
        public int id { get; set; }

        [DisplayName("Job Position")]
        [Required(ErrorMessage ="Please enter position name")]
        public string job { get; set; }

        [DisplayName("Basic Salary")]
        [Required(ErrorMessage = "Please enter basic salary")]
        [Range(0,9999.99)]
        public double basicSalary { get; set; }
    }
}
