﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models.ViewModel
{
    public class Payslip
    {
        [Required]
        public int SalaryId { get; set; }

        [DisplayName("Employee")]
        [Required]
        public string emp { get; set; }

        [DisplayName("Bonus")]
        public double bamount { get; set; }

        [DisplayName("Allowance")]
        public double aamount { get; set; }

        [DisplayName("Deduct")]
        public double damount { get; set; }

        [DisplayName("Month")]
        [Required]
        public string ym { get; set; }

        [DisplayName("EPF")]
        public double epf { get; set; }

        [DisplayName("Socso")]
        public double socso { get; set; }

        [DisplayName("Gross Salary")]
        [Required]
        public double grossSalary { get; set; }

        [DisplayName("Net Salary")]
        [Required]
        public double netSalary { get; set; }

        public List<avalue> avalue { get; set; }

        public List<bvalue> bvalue { get; set; }

        public List<dvalue> dvalue { get; set; }

        [NotMapped]
        public Position positions { get; set; }

        [NotMapped]
        public Employee employee { get; set; }

        public List<Allowance> allowance { get; set; }

        [NotMapped]
        public List<Bonus> bonus { get; set; }

        [NotMapped]
        public List<Deduct> deduct { get; set; }

        public double earning { get; set; }

        public double ded { get; set; }

    }
}
