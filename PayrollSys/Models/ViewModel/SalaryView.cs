﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models.ViewModel
{
    public class SalaryView
    {
        [Required]
        public int Id { get; set; }

        [DisplayName("Employee")]
        [Required]
        public string emp { get; set; }


        public double bamount { get; set; }


        public double aamount { get; set; }


        public double damount { get; set; }

        [DisplayName("Month")]
        [Required]
        public string ym { get; set; }

        public double epf { get; set; }


        public double socso { get; set; }

        [Required]
        public double grossSalary { get; set; }

        [Required]
        public double netSalary { get; set; }

        public List<AllowanceCheckBoxItem> allowancelist { get; set; }

        public List<BonusCheckBoxItem> bonuslist { get; set; }

        public List<DeductCheckBoxItem> deductlist { get; set; }

        [NotMapped]
        public List<SelectListItem> epflist { get; set; }

        [NotMapped]
        public Position positions { get; set; }

        [NotMapped]
        public int epfid { get; set; }

        

    }
}
