﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class bvalue
    {
        [Required]
        public int salaryId { get; set; }

        public Bonus Bonus { get; set; }

        [Required]
        public int bonusId { get; set; }

        public Salary Salary { get; set; }
    }
}
