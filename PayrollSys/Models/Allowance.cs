﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class Allowance
    {
        [Required]
        public int AllowanceId { get; set; }

        [Required]
        public string criteria { get; set; }

        [Required]
        public double rate { get; set; }

        public string note { get; set; }

        public List<avalue> avalue { get; set; }
    }
}
