﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PayrollSys.Models
{
    public class Attendance
    {
        public int ID { get; set; }
        public string Date { get; set; }
        public string emp { get; set; }
        public string Month { get; set; }
        public string Status { get; set; }


        //public enum AttdStatus
        //{
        //    present,
        //    absent,
        //    late
        //}
    }
}
