﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    
    public class avalue
    {

        [Required]
        public int salaryId { get; set; }

        public Allowance Allowance { get; set; }

        [Required]
        public int allowanceId { get; set; }

        public Salary Salary { get; set; }
    }
}
