﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class dvalue
    {
        [Required]
        public int salaryId { get; set; }

        public Deduct Deduct { get; set; }

        [Required]
        public int deductId { get; set; }

        public Salary Salary { get; set; }
    }
}
