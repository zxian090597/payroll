﻿using System;
using System.Collections.Generic;
using PayrollSys.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class AttdTable
    {
        public int ID { get; set; }
        public string Date { get; set; }
        public int Present { get; set; }
    }
}
