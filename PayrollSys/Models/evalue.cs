﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class evalue
    {
        [Required]
        public int salaryId { get; set; }

        public EPF ePF { get; set; }

        [Required]
        public int epfId { get; set; }

        public Salary Salary { get; set; }

        [Required]
        public string ym { get; set; }

        [Required]
        public double employee { get; set; }

        [Required]
        public double employer { get; set; }

    }
}
