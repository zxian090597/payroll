﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollSys.Models
{
    public class Employee
    {
        [Required]
        public int Id { get; set; }

        [DisplayName("Employee Name")]
        [Required(ErrorMessage = "Please enter employee name")]
        public string fullName { get; set; }

        [DisplayName("Password")]
        [Required(ErrorMessage = "Please enter password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Email (to be used as username)")]
        [Required(ErrorMessage = "Please enter email")]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "Please enter Phone Number")]
        public string PhoneNo { get; set; }

        [DisplayName("Address")]
        [Required(ErrorMessage = "Please enter address")]
        public string address { get; set; }

        [DisplayName("I/C No")]
        [Required(ErrorMessage = "Please enter IC No")]
        public string ICNo { get; set; }

        [DisplayName("Gender")]
        [Required(ErrorMessage = "Please select gender")]
        public string Gender { get; set; }

        [DisplayName("Religion")]
        [Required(ErrorMessage = "Please select religion")]
        public string religion { get; set; }

        [DisplayName("Position")]
        [Required(ErrorMessage = "Please select position")]
        public int PositionID { get; set; }

        [NotMapped]
        public List<Attendance> personalAttd { get; set; }

        [NotMapped]
        public List<Salary> personalSalary { get; set; }

        [Required]
        public string RoleName { get; set; }

        [NotMapped]
        public Position positions { get; set; }

        [NotMapped]
        public List<SelectListItem> PositionsOptions { get; set; }
        
    }
}
