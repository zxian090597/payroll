﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PayrollSys.Data.Migrations
{
    public partial class zxpart : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "empId",
                table: "attendance");

            migrationBuilder.AddColumn<string>(
                name: "Month",
                table: "attendance",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "advances",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    employeeID = table.Column<int>(nullable: false),
                    ym = table.Column<string>(nullable: false),
                    status = table.Column<string>(nullable: false),
                    employeeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_advances", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Allowance",
                columns: table => new
                {
                    AllowanceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    criteria = table.Column<string>(nullable: false),
                    rate = table.Column<double>(nullable: false),
                    note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Allowance", x => x.AllowanceId);
                });

            migrationBuilder.CreateTable(
                name: "Bonus",
                columns: table => new
                {
                    BonusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    criteria = table.Column<string>(nullable: false),
                    rate = table.Column<double>(nullable: false),
                    note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bonus", x => x.BonusId);
                });

            migrationBuilder.CreateTable(
                name: "Deduct",
                columns: table => new
                {
                    DeductId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    criteria = table.Column<string>(nullable: false),
                    rate = table.Column<double>(nullable: false),
                    note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deduct", x => x.DeductId);
                });

            migrationBuilder.CreateTable(
                name: "EPF",
                columns: table => new
                {
                    EPFId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Employee = table.Column<double>(nullable: false),
                    Employer = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EPF", x => x.EPFId);
                });

            migrationBuilder.CreateTable(
                name: "Salary",
                columns: table => new
                {
                    SalaryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    emp = table.Column<string>(nullable: false),
                    bamount = table.Column<double>(nullable: false),
                    aamount = table.Column<double>(nullable: false),
                    damount = table.Column<double>(nullable: false),
                    ym = table.Column<string>(nullable: false),
                    epf = table.Column<double>(nullable: false),
                    socso = table.Column<double>(nullable: false),
                    grossSalary = table.Column<double>(nullable: false),
                    netSalary = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Salary", x => x.SalaryId);
                });

            migrationBuilder.CreateTable(
                name: "Socso",
                columns: table => new
                {
                    SocsoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Pay_From = table.Column<double>(nullable: false),
                    Pay_To = table.Column<double>(nullable: false),
                    Employee = table.Column<double>(nullable: false),
                    Employer = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socso", x => x.SocsoId);
                });

            migrationBuilder.CreateTable(
                name: "avalue",
                columns: table => new
                {
                    salaryId = table.Column<int>(nullable: false),
                    allowanceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_avalue", x => new { x.salaryId, x.allowanceId });
                    table.ForeignKey(
                        name: "FK_avalue_Allowance_allowanceId",
                        column: x => x.allowanceId,
                        principalTable: "Allowance",
                        principalColumn: "AllowanceId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_avalue_Salary_salaryId",
                        column: x => x.salaryId,
                        principalTable: "Salary",
                        principalColumn: "SalaryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bvalue",
                columns: table => new
                {
                    salaryId = table.Column<int>(nullable: false),
                    bonusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bvalue", x => new { x.salaryId, x.bonusId });
                    table.ForeignKey(
                        name: "FK_bvalue_Bonus_bonusId",
                        column: x => x.bonusId,
                        principalTable: "Bonus",
                        principalColumn: "BonusId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bvalue_Salary_salaryId",
                        column: x => x.salaryId,
                        principalTable: "Salary",
                        principalColumn: "SalaryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "dvalue",
                columns: table => new
                {
                    salaryId = table.Column<int>(nullable: false),
                    deductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dvalue", x => new { x.salaryId, x.deductId });
                    table.ForeignKey(
                        name: "FK_dvalue_Deduct_deductId",
                        column: x => x.deductId,
                        principalTable: "Deduct",
                        principalColumn: "DeductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dvalue_Salary_salaryId",
                        column: x => x.salaryId,
                        principalTable: "Salary",
                        principalColumn: "SalaryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "evalue",
                columns: table => new
                {
                    salaryId = table.Column<int>(nullable: false),
                    epfId = table.Column<int>(nullable: false),
                    ym = table.Column<string>(nullable: false),
                    employee = table.Column<double>(nullable: false),
                    employer = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_evalue", x => new { x.salaryId, x.epfId });
                    table.ForeignKey(
                        name: "FK_evalue_EPF_epfId",
                        column: x => x.epfId,
                        principalTable: "EPF",
                        principalColumn: "EPFId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_evalue_Salary_salaryId",
                        column: x => x.salaryId,
                        principalTable: "Salary",
                        principalColumn: "SalaryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "svalue",
                columns: table => new
                {
                    salaryId = table.Column<int>(nullable: false),
                    socsoId = table.Column<int>(nullable: false),
                    ym = table.Column<string>(nullable: false),
                    employee = table.Column<double>(nullable: false),
                    employer = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_svalue", x => new { x.salaryId, x.socsoId });
                    table.ForeignKey(
                        name: "FK_svalue_Salary_salaryId",
                        column: x => x.salaryId,
                        principalTable: "Salary",
                        principalColumn: "SalaryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_svalue_Socso_socsoId",
                        column: x => x.socsoId,
                        principalTable: "Socso",
                        principalColumn: "SocsoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_avalue_allowanceId",
                table: "avalue",
                column: "allowanceId");

            migrationBuilder.CreateIndex(
                name: "IX_bvalue_bonusId",
                table: "bvalue",
                column: "bonusId");

            migrationBuilder.CreateIndex(
                name: "IX_dvalue_deductId",
                table: "dvalue",
                column: "deductId");

            migrationBuilder.CreateIndex(
                name: "IX_evalue_epfId",
                table: "evalue",
                column: "epfId");

            migrationBuilder.CreateIndex(
                name: "IX_svalue_socsoId",
                table: "svalue",
                column: "socsoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "advances");

            migrationBuilder.DropTable(
                name: "avalue");

            migrationBuilder.DropTable(
                name: "bvalue");

            migrationBuilder.DropTable(
                name: "dvalue");

            migrationBuilder.DropTable(
                name: "evalue");

            migrationBuilder.DropTable(
                name: "svalue");

            migrationBuilder.DropTable(
                name: "Allowance");

            migrationBuilder.DropTable(
                name: "Bonus");

            migrationBuilder.DropTable(
                name: "Deduct");

            migrationBuilder.DropTable(
                name: "EPF");

            migrationBuilder.DropTable(
                name: "Salary");

            migrationBuilder.DropTable(
                name: "Socso");

            migrationBuilder.DropColumn(
                name: "Month",
                table: "attendance");

            migrationBuilder.AddColumn<string>(
                name: "empId",
                table: "attendance",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
