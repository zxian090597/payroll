﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Models;

namespace PayrollSys.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Employee> employees { get; set; }

        public DbSet<Admin> admins { get; set; }

        public DbSet<SuperAdmin> superadmins { get; set; }

        public DbSet<Position> positions { get; set; }

        public DbSet<Attendance> attendance { get; set; }

        public DbSet<AttdTable> attdTable { get; set; }

        public DbSet<PayrollSys.Models.Bonus> Bonus { get; set; }
        public DbSet<PayrollSys.Models.Allowance> Allowance { get; set; }
        public DbSet<PayrollSys.Models.Deduct> Deduct { get; set; }
        public DbSet<PayrollSys.Models.Socso> Socso { get; set; }
        public DbSet<PayrollSys.Models.EPF> EPF { get; set; }
        public DbSet<PayrollSys.Models.Salary> Salary { get; set; }

        public DbSet<PayrollSys.Models.avalue> avalue { get; set; }

        public DbSet<PayrollSys.Models.bvalue> bvalue { get; set; }

        public DbSet<PayrollSys.Models.dvalue> dvalue { get; set; }

        public DbSet<PayrollSys.Models.evalue> evalue { get; set; }

        public DbSet<PayrollSys.Models.svalue> svalue { get; set; }

        public DbSet<Advance> advances { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<avalue>()
                .HasKey(a => new { a.salaryId, a.allowanceId });
            builder.Entity<avalue>()
                .HasOne(s => s.Salary)
                .WithMany(a => a.avalue)
                .HasForeignKey(s => s.salaryId);
            builder.Entity<avalue>()
                .HasOne(s => s.Allowance)
                .WithMany(t => t.avalue)
                .HasForeignKey(s => s.allowanceId);
            builder.Entity<bvalue>()
                .HasKey(b => new { b.salaryId, b.bonusId });
            builder.Entity<bvalue>()
                .HasOne(s => s.Salary)
                .WithMany(b => b.bvalue)
                .HasForeignKey(s => s.salaryId);
            builder.Entity<bvalue>()
                .HasOne(s => s.Bonus)
                .WithMany(t => t.bvalue)
                .HasForeignKey(s => s.bonusId);
            builder.Entity<dvalue>()
                .HasKey(d => new { d.salaryId, d.deductId });
            builder.Entity<dvalue>()
                .HasOne(s => s.Salary)
                .WithMany(d => d.dvalue)
                .HasForeignKey(s => s.salaryId);
            builder.Entity<dvalue>()
                .HasOne(s => s.Deduct)
                .WithMany(t => t.dvalue)
                .HasForeignKey(s => s.deductId);
            builder.Entity<evalue>()
                .HasKey(e => new { e.salaryId, e.epfId });
            builder.Entity<evalue>()
                .HasOne(s => s.Salary)
                .WithMany(e => e.evalue)
                .HasForeignKey(s => s.salaryId);
            builder.Entity<evalue>()
                .HasOne(s => s.ePF)
                .WithMany(t => t.evalue)
                .HasForeignKey(s => s.epfId);
            builder.Entity<svalue>()
                .HasKey(s => new { s.salaryId, s.socsoId });
            builder.Entity<svalue>()
                .HasOne(s => s.Salary)
                .WithMany(s => s.svalue)
                .HasForeignKey(s => s.salaryId);
            builder.Entity<svalue>()
                .HasOne(s => s.socso)
                .WithMany(t => t.svalue)
                .HasForeignKey(s => s.socsoId);

        }

    }
}
