﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Data;
using PayrollSys.Models;

namespace PayrollSys.Controllers
{
    public class BasicsalaryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BasicsalaryController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Basicsalary
        public async Task<IActionResult> Index()
        {
            
            return View("~/Views/Admin/managesalary/Basicsalary/Index.cshtml",await _context.positions.ToListAsync());
        }

        
        // GET: Basicsalary/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/managesalary/Basicsalary/Create.cshtml");
        }

        // POST: Basicsalary/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,job,basicSalary")] Position pos)
        {
            //pos.job = char.ToUpper(pos.job[0]) + pos.job.Substring(1); 
            if (ModelState.IsValid)
            {
                _context.Add(pos);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Basicsalary");
            }
            return View("~/Views/Admin/managesalary/Basicsalary/Create.cshtml",pos);
        }

        // GET: Basicsalary/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var position = await _context.positions.FindAsync(id);
            if (position == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/managesalary/Basicsalary/Edit.cshtml",position);
        }

        // POST: Basicsalary/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,job,basicSalary")] Position pos)
        {
            if (id != pos.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    pos.job = char.ToUpper(pos.job[0]) + pos.job.Substring(1);
                    _context.Update(pos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!positionExists(pos.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Basicsalary/Edit.cshtml",pos);
        }

        // GET: Basicsalary/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var position = await _context.positions
                .FirstOrDefaultAsync(m => m.id == id);
            if (position == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Basicsalary/Delete.cshtml",position);
        }

        // POST: Basicsalary/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var position = await _context.positions.FindAsync(id);
            _context.positions.Remove(position);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool positionExists(int id)
        {
            return _context.positions.Any(e => e.id == id);
        }
    }
}
