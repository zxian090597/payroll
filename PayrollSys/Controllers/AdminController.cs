﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PayrollSys.Models;
using PayrollSys.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace PayrollSys.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        
        private UserManager<AppUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private IPasswordHasher<AppUser> _passwordHasher;
        private ApplicationDbContext _context;

        public AdminController(UserManager<AppUser> _userMgr, 
            RoleManager<IdentityRole> roleManager, 
            IPasswordHasher<AppUser> passwordHash,
            ApplicationDbContext context)
        {
            _userManager = _userMgr;
            _roleManager = roleManager;
            _passwordHasher = passwordHash;
            _context = context;

        }
        
        //The main page of admin user
        public IActionResult Index()
        {
            return View();
        }

        
        //To list out details of attendance based on dates
        public IActionResult Attendance()
        {
            var attdTable = _context.attdTable.ToList();
            return View(attdTable);
        }

        
        //To create a new date based on current date
        public IActionResult NewDate(DateTime CustomDate)
        {
            string today;
            string month;
            if (CustomDate == null)
            {
                today = DateTime.Today.ToString("dd/MM/yyyy");
                month = DateTime.Today.ToString("yyyy-MM");
            }
            else
            {
                today = CustomDate.ToString("dd/MM/yyyy");
                month = CustomDate.ToString("yyyy-MM");
            }
            
            
            //DateTime baru = new DateTime(2020, 6, 1);
            //string today = baru.ToString("dd/MM/yyyy");
            var checkDate = _context.attdTable.ToList();
            bool existDate = false;
            foreach (AttdTable attd in checkDate)      //To check for duplicate entries of same date
            {
                if (attd.Date == today)
                {
                    existDate = true;
                }

            }
            if (existDate)  //Redirect to exist date if duplicate entries are performed
            {
                ModelState.AddModelError("", "This date already exists.");
                return RedirectToAction("ViewAttendance", "Admin", new { date = today});
            }
            AttdTable attdTable = new AttdTable
            {
                Date = today,
                Present = 0
            };
            var newList = _context.employees.ToList();
            foreach (var user in newList)       //Create list of employees with initial status set to absent
            {
                Models.Attendance attd = new Models.Attendance
                {
                    Date = today,
                    Month = month,
                    emp = user.Email,
                    Status = "Absent"
                };
                _context.Add(attd);
            }
            _context.SaveChanges();
            _context.Add(attdTable);
            _context.SaveChanges();
            return RedirectToAction("Attendance");
        }


        //To mark the employee as present
        [HttpPost]
        public IActionResult ChangeToPresent(int id)
        {
            Models.Attendance attd = _context.attendance.SingleOrDefault(x => x.ID == id);
            string today = attd.Date;
            AttdTable att = _context.attdTable.SingleOrDefault(x => x.Date == today);
            if(attd.Status == "Present")
            {
                return RedirectToAction("ViewAttendance", "Admin", new { date = today });
            }
            else if (attd.Status == "Absent")
            {
                att.Present += 1;
            }
            attd.Status = "Present";
            _context.SaveChanges();
            return RedirectToAction("ViewAttendance", "Admin", new { date = today });
        }

        [HttpPost]
        public IActionResult ChangeToLate(int id)
        {
            Models.Attendance attd = _context.attendance.SingleOrDefault(x => x.ID == id);
            string today = attd.Date;
            AttdTable att = _context.attdTable.SingleOrDefault(x => x.Date == today);
            if (attd.Status == "Late")
            {
                return RedirectToAction("ViewAttendance", "Admin", new { date = today });
            }
            else if (attd.Status == "Absent")
            {
                att.Present += 1;
            }
            attd.Status = "Late";
            _context.SaveChanges();
            return RedirectToAction("ViewAttendance", "Admin", new { date = today });
        }

        [HttpPost]
        public IActionResult DeletePeople(int id)
        {
            Models.Attendance attd = _context.attendance.SingleOrDefault(x => x.ID == id);
            string today = attd.Date;

            if (attd != null)
            {
                _context.attendance.Remove(attd);
                _context.SaveChanges();
                return RedirectToAction("ViewAttendance", "Admin", new { date = today });
            }
            else
            {
                return RedirectToAction("ViewAttendance", "Admin", new { date = today });
            }
        }

        [HttpPost]
        public IActionResult DeleteAttendance(int id)
        {
            AttdTable attd = _context.attdTable.SingleOrDefault(x => x.ID == id);
            string date = attd.Date;
            var attList = _context.attendance.Where(x => x.Date == date).ToList();
            if (attd != null)
            {
                foreach(var emp in attList)
                {
                    _context.attendance.Remove(emp);
                }
                _context.SaveChanges();
                _context.attdTable.Remove(attd);
                _context.SaveChanges();
                return RedirectToAction("Attendance");
            }
            else
            {
                return RedirectToAction("Attendance");
            }
        }

        [HttpGet]
        public IActionResult ViewAttendance(string date)
        {
            var attList = _context.attendance.Where(x => x.Date == date).ToList();
            return View(attList);
        }

        public IActionResult ListOut()
        {
            var ppl = _context.employees.ToList();
            foreach (var emp in ppl)
            {
                if (emp.PositionID == 0)
                {
                    emp.PositionID = 3;
                }
                Models.Position post = _context.positions.Where(x => x.id == emp.PositionID).First();
                if(post == null)
                {
                    return RedirectToAction("Index");
                }
                emp.positions = post;
            }
            return View(ppl);
        }

        public IActionResult CreateEmployee()
        {
            Models.Employee emp = new Models.Employee();
            var post = _context.positions.ToList();
            emp.PositionsOptions = _context.positions.Select(a =>
            new SelectListItem
            {
                Value = a.id.ToString(),
                Text = a.job
            }).ToList();
            return View(emp);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployee(Models.Employee emp)
        {
            if (ModelState.IsValid)
            {
                var role = _roleManager.FindByNameAsync(emp.RoleName).Result;
                AppUser appUser = new AppUser
                {
                    UserName = emp.Email,
                    Email = emp.Email
                };

                IdentityResult result = await _userManager.CreateAsync(appUser, emp.Password);
                if (result.Succeeded)
                {
                    Models.Employee ppl = new Models.Employee
                    {
                        fullName = emp.fullName,
                        Password = emp.Password,
                        Email = emp.Email,
                        PhoneNo = emp.PhoneNo,
                        address = emp.address,
                        ICNo = emp.ICNo,
                        Gender = emp.Gender,
                        religion = emp.religion,
                        PositionID = emp.PositionID,
                        RoleName = emp.RoleName
                    };
                    _context.Add(ppl);
                    _context.SaveChanges();
                    await _userManager.AddToRoleAsync(appUser, role.Name);
                    return RedirectToAction("Listout");
                }
                else
                {
                    foreach (IdentityError error in result.Errors)
                        ModelState.AddModelError("", error.Description);
                }
            }
            var post = _context.positions.ToList();
            emp.PositionsOptions = _context.positions.Select(a =>
            new SelectListItem
            {
                Value = a.id.ToString(),
                Text = a.job
            }).ToList();
            return View(emp);
        }

        [HttpGet]
        public IActionResult UpdateEmployee(int id)
        {
            Models.Employee emp = _context.employees.Where(x => x.Id == id).First();
            if (emp != null)
            {
                var post = _context.positions.ToList();
                emp.PositionsOptions = _context.positions.Select(a =>
                new SelectListItem
                {
                    Value = a.id.ToString(),
                    Text = a.job
                }).ToList();
                return View(emp);
            }
            else
                ModelState.AddModelError("", "User Not Found");
            return RedirectToAction("Listout");
        }

        [HttpPost]
        public IActionResult UpdateEmployee(Models.Employee y)
        {
            if (ModelState.IsValid)
            {
                Models.Employee emp = _context.employees.Where(x => x.Id == y.Id).FirstOrDefault();
                if(emp != null)
                {
                    emp.fullName = y.fullName;
                    emp.Password = y.Password;
                    emp.Email = y.Email;
                    emp.PhoneNo = y.PhoneNo;
                    emp.address = y.address;
                    emp.ICNo = y.ICNo;
                    emp.Gender = y.Gender;
                    emp.religion = y.religion;
                    emp.PositionID = y.PositionID;
                    _context.SaveChanges();
                }
                return RedirectToAction("Listout");
            }
            else
            {
                ModelState.AddModelError("", "error!");
                return View(y);
            }
        }

        private void Errors(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
                ModelState.AddModelError("", error.Description);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteEmployee(string email)
        {
            AppUser user = await _userManager.FindByNameAsync(email);
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    Models.Employee emp = _context.employees.Where(y => y.Email == email).First();
                    if (emp != null)
                    {
                        _context.employees.Remove(emp);
                        _context.SaveChanges();
                    }
                    return RedirectToAction("Listout");
                }  
                else
                    Errors(result);
            }
            else
                ModelState.AddModelError("", "User Not Found");
            return View("Index", _userManager.Users);
        }

        

        [HttpGet]
        public IActionResult EmployeeAttendance(int id)
        {
            Models.Employee employ = _context.employees.Where(x => x.Id == id).First();
            if(employ == null)
            {
                return RedirectToAction("Listout");
            }
            List<Models.Attendance> att = _context.attendance.Where(y => y.emp == employ.Email && y.Status != null).ToList();
            if(att == null)
            {
                return RedirectToAction("Listout");
            }
            return View(att);
        }

        public ViewResult managesalary() => View("~/Views/Admin/managesalary/managesalary.cshtml");

        public IActionResult ManageAdvance()
        {
            
                var adv = _context.advances.ToList();
                return View(adv);
            
        }

        [HttpPost]
        public ActionResult ManageAdvance(string month)
        {
                var adv = _context.advances.Where(x => x.ym == month).ToList();
                return View(adv);
            
        }

        public IActionResult ApproveRequest(int id)
        {
            Advance a = _context.advances.Where(x => x.id == id).First();
            string bulan = a.ym;
            a.status = "Approved";
            _context.SaveChanges();
            return RedirectToAction("ManageAdvance", new { ym = bulan});
        }

        public IActionResult RejectRequest(int id)
        {
            Advance a = _context.advances.Where(x => x.id == id).First();
            string bulan = a.ym;
            a.status = "Rejected";
            _context.SaveChanges();
            return RedirectToAction("ManageAdvance", new { ym = bulan});
        }

        public IActionResult Report()
        {
            string today = DateTime.Today.ToString("yyyy-MM");
            List<evalue> evalues = _context.evalue.Where(x => x.ym == today).ToList();
            List<svalue> svalues = _context.svalue.Where(x => x.ym == today).ToList();
            double epf = 0;
            double socso = 0;
            double totalnet = 0;
            foreach (var item in evalues)
            {
                epf += item.employer;
            }
            foreach (var item in svalues)
            {
                socso += item.employer;
            }
            List<Report> taxreports = new List<Report>();
            taxreports.Add(new Models.Report("EPF", epf));
            taxreports.Add(new Models.Report("Socso", socso));

            List<Salary> salaries = _context.Salary.Where(x => x.ym == today).ToList();
            List<Report> slyreports = new List<Report>();
            foreach (var item in salaries)
            {
                Models.Employee ppl = _context.employees.Where(x => x.Email == item.emp).First();
                slyreports.Add(new Models.Report(ppl.fullName, item.netSalary));
                totalnet += item.netSalary;
            }

            ViewBag.TaxReports = JsonConvert.SerializeObject(taxreports);
            ViewBag.SlyReports = JsonConvert.SerializeObject(slyreports);
            ViewBag.Epf = epf;
            ViewBag.Socso = socso;
            ViewBag.Tnet = Math.Round(totalnet, 2);
            ViewBag.Month = today;

            return View();
        }

        [HttpPost]
        public IActionResult Report(string month)
        {
            string today = month;
            List<evalue> evalues = _context.evalue.Where(x => x.ym == today).ToList();
            List<svalue> svalues = _context.svalue.Where(x => x.ym == today).ToList();
            double epf = 0;
            double socso = 0;
            double totalnet = 0;
            foreach (var item in evalues)
            {
                epf += item.employer;
            }
            foreach (var item in svalues)
            {
                socso += item.employer;
            }
            List<Report> taxreports = new List<Report>();
            taxreports.Add(new Models.Report("EPF", epf));
            taxreports.Add(new Models.Report("Socso", socso));

            List<Salary> salaries = _context.Salary.Where(x => x.ym == today).ToList();
            List<Report> slyreports = new List<Report>();
            foreach (var item in salaries)
            {
                Models.Employee ppl = _context.employees.Where(x => x.Email == item.emp).First();
                slyreports.Add(new Models.Report(ppl.fullName, item.netSalary));
                totalnet += item.netSalary;
            }

            ViewBag.TaxReports = JsonConvert.SerializeObject(taxreports);
            ViewBag.SlyReports = JsonConvert.SerializeObject(slyreports);
            ViewBag.Epf = epf;
            ViewBag.Socso = socso;
            ViewBag.Tnet = Math.Round(totalnet, 2);
            ViewBag.Month = today;

            return View();
        }
    }
}