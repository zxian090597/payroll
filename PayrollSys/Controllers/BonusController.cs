﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Data;
using PayrollSys.Models;

namespace PayrollSys.Controllers
{
    public class BonusController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BonusController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Bonus
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/managesalary/Bonus/Index.cshtml",await _context.Bonus.ToListAsync());
        }

        // GET: Bonus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bonus = await _context.Bonus
                .FirstOrDefaultAsync(m => m.BonusId == id);
            if (bonus == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Bonus/Details.cshtml",bonus);
        }

        // GET: Bonus/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/managesalary/Bonus/Create.cshtml");
        }

        // POST: Bonus/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BonusId,criteria,rate,note")] Bonus bonus)
        {
            if (ModelState.IsValid)
            {
                bonus.criteria = char.ToUpper(bonus.criteria[0]) + bonus.criteria.Substring(1);
                bonus.note = char.ToUpper(bonus.note[0]) + bonus.note.Substring(1);
                _context.Add(bonus);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Bonus");
            }
            return View("~/Views/Admin/managesalary/Bonus/Create.cshtml",bonus);
        }

        // GET: Bonus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bonus = await _context.Bonus.FindAsync(id);
            if (bonus == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/managesalary/Bonus/Edit.cshtml",bonus);
        }

        // POST: Bonus/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BonusId,criteria,rate,note")] Bonus bonus)
        {
            if (id != bonus.BonusId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    bonus.criteria = char.ToUpper(bonus.criteria[0]) + bonus.criteria.Substring(1);
                    bonus.note = char.ToUpper(bonus.note[0]) + bonus.note.Substring(1);
                    _context.Update(bonus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BonusExists(bonus.BonusId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Bonus/Edit.cshtml",bonus);
        }

        // GET: Bonus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bonus = await _context.Bonus
                .FirstOrDefaultAsync(m => m.BonusId == id);
            if (bonus == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Bonus/Delete.cshtml",bonus);
        }

        // POST: Bonus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bonus = await _context.Bonus.FindAsync(id);
            _context.Bonus.Remove(bonus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BonusExists(int id)
        {
            return _context.Bonus.Any(e => e.BonusId == id);
        }
    }
}
