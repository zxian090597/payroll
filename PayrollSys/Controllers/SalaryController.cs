﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Data;
using PayrollSys.Models;
using Microsoft.AspNetCore.Identity;
using PayrollSys.Models.ViewModel;
using System.Collections;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace PayrollSys.Controllers
{
    public class SalaryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SalaryController(ApplicationDbContext context)
        {
            _context = context;
        }

        public ActionResult Index()
        {

            List<string> sList = new List<string>();

            foreach (Salary row in _context.Salary)
            {
                sList.Add(row.ym.ToString());
            }

            return View("~/Views/Admin/calculatesalary/Index.cshtml", new HashSet<string>(sList));
        }

        public IActionResult NewMonth(string month)
        {
            if (month == null)
            {
                ModelState.AddModelError("", "error.");
                return RedirectToAction("Index", "Salary");
            }
            evalue el = new evalue();
            svalue sl = new svalue();
            var salary = _context.Salary.ToList();
            bool existmonth = false;

            foreach (var item in salary)      //To check for duplicate entries of same month
            {
                if (item.ym == month)
                {
                    existmonth = true;
                }

            }
            if (existmonth)  //Redirect to exist month if duplicate entries are performed
            {
                ModelState.AddModelError("", "This salary of this month already exists.");
                return RedirectToAction("Index", "Salary");
            }
            

                var newList = _context.employees.ToList();

                foreach (var user in newList)       //Create list of employees with initial status set to absent
                {
                    //calculate adsent day
                    var at = _context.attendance.Where(x => x.emp == user.Email && x.Month == month).ToList();
                    int counter = 0;
                    foreach (var day in at)
                    {
                        if (day.Status == "Absent")
                        {
                            counter++;
                        }
                    }

                    // verify whether got advance salary
                    var av = _context.advances.Where(x => x.employeeName == user.Email && x.ym == month).ToList();
                    double advance = 0;
                    foreach (var item in av)
                    {
                        if (item.status=="Approved")
                        {
                            advance = 1;
                        }
                    }

                    //find basic salary
                    Models.Position post = _context.positions.Where(x => x.id == user.PositionID).First();
                    double bs = post.basicSalary;

                    // epf
                    Models.EPF epf = _context.EPF.Where(x => x.EPFId == 1).First();

                    //socso
                    Models.Socso s = _context.Socso.Where(m => m.Pay_From <= bs && m.Pay_To >= bs).First();

                    // calculate deduction
                    Models.Deduct d = _context.Deduct.Where(d => d.DeductId == 1).First();
                    Models.Deduct de = _context.Deduct.Where(d => d.DeductId == 2).First();
                    double deduct = 0;
                    double asbent = 0;
                    double ad = 0;
                    if (counter > 0)
                    {
                        asbent = bs / d.rate * counter;
                    }
                    if (advance == 1)
                    {
                        ad = bs * de.rate / 100;

                    }
                    deduct = asbent + ad;
                    Math.Round(deduct, 2);
                    
                    // calculate netsalary
                    double net =0;
                    net = bs - (epf.Employee / 100 * bs) - s.Employee - deduct;
                    //create salary
                    Models.Salary sa = new Models.Salary
                    {

                        emp = user.Email,
                        bamount = 0,
                        aamount = 0,
                        damount = Math.Round(deduct, 2),
                        ym = month,
                        epf = Math.Round(epf.Employee / 100 * bs,2),
                        socso = s.Employee,
                        grossSalary = bs,
                        netSalary = Math.Round(net, 2),

                    };
                    _context.Add(sa);
                    _context.SaveChanges();

                    int salaryid = sa.SalaryId;
                    // add value to evalue
                    el.epfId = epf.EPFId;
                    el.salaryId = salaryid;
                    el.ym = sa.ym;
                    el.employee = sa.epf;
                    el.employer = epf.Employer / 100 * bs;
                    _context.evalue.Add(el);
                    _context.SaveChanges();

                    // add value to svalue
                    sl.socsoId = s.SocsoId;
                    sl.salaryId = salaryid;
                    sl.ym = sa.ym;
                    sl.employee = sa.socso;
                    sl.employer = s.Employer;
                    _context.svalue.Add(sl);
                    _context.SaveChanges();

                    // add value to dvalue
                    List<dvalue> dlc = new List<dvalue>();
                    if (counter > 0)
                    {
                        dlc.Add(new dvalue() { salaryId = salaryid, deductId = 1 });
                    }
                    if (advance == 1)
                    {
                        dlc.Add(new dvalue() { salaryId = salaryid, deductId = 2 });

                    }
                    foreach (var item in dlc)
                    {
                        _context.dvalue.Add(item);
                        _context.SaveChanges();
                    }

                }

            return RedirectToAction("Index");
            
        }
       
        public  IActionResult List(string ym)
        {

            if (ym == null)
            {
                return NotFound();
            }
            var salary = _context.Salary
                .Where( m=>m.ym == ym)
                .ToList();
            if (salary == null)
            {
                return NotFound();
            }
            foreach (var item in salary)
            {
                Models.Employee ppl = _context.employees.Where(x => x.Email == item.emp).First();
                if (ppl == null)
                {
                    return RedirectToAction("Index");
                }
                item.employee = ppl;
                Models.Position post = _context.positions.Where(x => x.id == ppl.PositionID).First();
                item.positions = post;
            }

            return View("~/Views/Admin/calculatesalary/List.cshtml",salary);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            SalaryView sal = new SalaryView();
            var a = _context.Salary.Include(s => s.avalue).ThenInclude(e => e.Allowance).AsNoTracking().SingleOrDefault(m => m.SalaryId == id);
            var b = _context.Salary.Include(s => s.bvalue).ThenInclude(e => e.Bonus).AsNoTracking().SingleOrDefault(m=>m.SalaryId == id);
            var d = _context.Salary.Include(s => s.dvalue).ThenInclude(e => e.Deduct).AsNoTracking().SingleOrDefault(m => m.SalaryId == id);

            // list allowance
            var aitem = _context.Allowance.Select(vm => new AllowanceCheckBoxItem()
            {
                AllowanceId = vm.AllowanceId,
                criteria = vm.criteria,
                rate=vm.rate,
                IsChecked = vm.avalue.Any(x=>x.salaryId==a.SalaryId)? true : false
            }).ToList();

            //list bonus
            var bitem = _context.Bonus.Select(vm => new BonusCheckBoxItem()
            {
                BonusId = vm.BonusId,
                criteria = vm.criteria,
                rate = vm.rate,
                IsChecked = vm.bvalue.Any(x=>x.salaryId==b.SalaryId)? true : false
            }).ToList();

            //list deduct
            var ditem = _context.Deduct.Select(vm => new DeductCheckBoxItem()
            {
                DeductId = vm.DeductId,
                criteria = vm.criteria,
                rate = vm.rate,
                IsChecked = vm.dvalue.Any(x => x.salaryId == d.SalaryId) ? true : false
            }).ToList();

            sal.emp = a.emp;
            sal.ym = a.ym;
            sal.allowancelist = aitem;
            sal.bonuslist = bitem;
            sal.deductlist = ditem;
            sal.grossSalary = a.grossSalary;
            return View("~/Views/Admin/calculatesalary/Edit.cshtml",sal);
        }

        [HttpPost]
        public IActionResult Edit(Models.ViewModel.SalaryView sv)
        {
            
            List<avalue> alc = new List<avalue>();
            List<bvalue> blc = new List<bvalue>();
            List<dvalue> dlc = new List<dvalue>();
            evalue el = new evalue();
            svalue sl = new svalue();
            if (ModelState.IsValid)
            {
                Salary salary = _context.Salary.SingleOrDefault(x => x.SalaryId == sv.Id);
                
                salary.emp = sv.emp;
                salary.ym = sv.ym;
                salary.grossSalary = sv.grossSalary;
                int salaryid = salary.SalaryId;
                double a = 0;
                double b = 0;
                string returnYM = salary.ym;

                //Get value from allowancecheckbox
                foreach (var item in sv.allowancelist)
                {
                    if (item.IsChecked == true)
                    {
                        alc.Add(new avalue() { salaryId = salaryid, allowanceId = item.AllowanceId });
                        a += item.rate;

                    }
                }

                var databaseavalue = _context.avalue.Where(a => a.salaryId == salaryid).ToList();
                if (databaseavalue != null)
                {
                    foreach (var item in databaseavalue)
                    {
                        _context.avalue.Remove(item);
                        _context.SaveChanges();
                    }
                }
                foreach (var item in alc)
                {
                    _context.avalue.Add(item);
                    _context.SaveChanges();
                }

                //Get value from bonuscheckbox
                foreach (var item in sv.bonuslist)
                {
                    if (item.IsChecked == true)
                    {
                        blc.Add(new bvalue() { salaryId = salaryid, bonusId = item.BonusId });
                        b += item.rate;

                    }
                }

                var databasebvalue = _context.bvalue.Where(a => a.salaryId == salaryid).ToList();
                if (databasebvalue != null)
                {
                    foreach (var item in databasebvalue)
                    {
                        _context.bvalue.Remove(item);
                        _context.SaveChanges();
                    }
                }
                foreach (var item in blc)
                {
                    _context.bvalue.Add(item);
                    _context.SaveChanges();
                }

                //Get value from deductcheckbox
                foreach (var item in sv.deductlist)
                {
                    if (item.IsChecked == true)
                    {
                        
                        dlc.Add(new dvalue() { salaryId = salaryid, deductId = item.DeductId });

                    }
                }

                var databasedvalue = _context.dvalue.Where(a => a.salaryId == salaryid).ToList();
                if (databasedvalue != null)
                {
                    foreach (var item in databasedvalue)
                    {
                        _context.dvalue.Remove(item);
                        _context.SaveChanges();
                    }
                }
                foreach (var item in dlc)
                {

                    _context.dvalue.Add(item);
                    _context.SaveChanges();
                }

                // calculation allowance, bonus
                salary.aamount = a;
                salary.bamount = Math.Round(b / 100 * salary.grossSalary,2);

                //calculation deduct
                var getd = _context.dvalue.Where(a => a.salaryId == salaryid).ToList();
                double deduct = 0; 
                foreach (var item in getd)
                {
                    if (item.deductId == 1)
                    {
                        //calculate adsent day
                        var at = _context.attendance.Where(x => x.emp == salary.emp && x.Month == salary.ym).ToList();
                        int counter = 0;
                        foreach (var day in at)
                        {
                            if (day.Status == "Absent")
                            {
                                counter++;
                            }
                        }

                        if (counter > 0)
                        {
                            var d = _context.Deduct.Where(x => x.DeductId == 1).First();
                            double absent = 0;
                            absent = salary.grossSalary / d.rate * counter;
                            deduct += absent;
                        }
                        
                    }
                    else if( item.deductId==2 )
                    {
                        // verify got request advance
                        var av = _context.advances.Where(x => x.employeeName == salary.emp && x.ym == salary.ym ).ToList();
                        foreach (var advance in av)
                        {
                            if (advance.status == "Approved")
                            {
                                var d = _context.Deduct.Where(x => x.DeductId == 2).First();
                                double dv = salary.grossSalary * d.rate / 100;
                                deduct += dv;
                            }
                        }

                    }
                    else
                    {
                        var d = _context.Deduct.Where(x => x.DeductId == item.deductId).First();
                        double dv = salary.grossSalary * d.rate / 100;
                        deduct += dv;
                    }


                }
                salary.damount = Math.Round(deduct,2);

                //calculate epf
                var e = _context.EPF.Where(x => x.EPFId == 1).First();
                double epfs = salary.grossSalary + salary.aamount + salary.bamount;
                salary.epf = Math.Round(e.Employee / 100 * epfs,2);

                //calculate socso
                double gs = salary.grossSalary + salary.aamount;
                var s = _context.Socso
                            .Where(m => m.Pay_From <= gs && m.Pay_To >= gs).First();
                salary.socso = s.Employee;

                // add value to evalue
                var getevalue = _context.evalue.Where(a => a.salaryId == salaryid).ToList();
                foreach (var item in getevalue)
                {
                    _context.evalue.Remove(item);
                    _context.SaveChanges();
                }

                el.epfId = e.EPFId;
                el.salaryId = salaryid;
                el.ym = salary.ym;
                el.employee = salary.epf;
                el.employer = e.Employer / 100 * epfs;
                _context.evalue.Add(el);
                _context.SaveChanges();


                // add value to svalue
                var getsvalue = _context.svalue.Where(a => a.salaryId == salaryid).ToList();
                foreach (var item in getsvalue)
                {

                    _context.svalue.Remove(item);
                    _context.SaveChanges();

                }
                sl.socsoId = s.SocsoId;
                sl.salaryId = salaryid;
                sl.ym = salary.ym;
                sl.employee = salary.socso;
                sl.employer = s.Employer;
                _context.svalue.Add(sl);
                _context.SaveChanges();
                double net = 0;
                net = salary.grossSalary + salary.aamount + salary.bamount - salary.damount - salary.epf - salary.socso;
                salary.netSalary = Math.Round(net, 2);

                _context.Salary.Update(salary);
                _context.SaveChanges();

                return RedirectToAction("List",new { ym = returnYM});
            }
            ModelState.AddModelError("", "error!");
            return RedirectToAction(nameof(Index));


        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salary = await _context.Salary
                .FirstOrDefaultAsync(m => m.SalaryId == id);
            if (salary == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/calculatesalary/Delete.cshtml",salary);
        }

        // POST: Salaries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var salary = await _context.Salary.FindAsync(id);
            string returnYM = salary.ym;
            _context.Salary.Remove(salary);
            await _context.SaveChangesAsync();
            var av = await _context.avalue.Where(a => a.salaryId == id).ToListAsync();
            if (av!=null)
            {
                foreach (var item in av)
                {
                    _context.avalue.Remove(item);
                    await _context.SaveChangesAsync();
                }

            }
            var bv = await _context.bvalue.Where(a => a.salaryId == id).ToListAsync();
            if (bv != null)
            {
                foreach (var item in bv)
                {
                    _context.bvalue.Remove(item);
                    await _context.SaveChangesAsync();
                }

            }
            var dv = await _context.dvalue.Where(a => a.salaryId == id).ToListAsync();
            if (dv != null)
            {
                foreach (var item in dv)
                {
                    _context.dvalue.Remove(item);
                    await _context.SaveChangesAsync();
                }

            }
            var ev = await _context.evalue.Where(a => a.salaryId == id).ToListAsync();
            if (ev != null)
            {
                foreach (var item in ev)
                {
                    _context.evalue.Remove(item);
                    await _context.SaveChangesAsync();
                }

            }
            var sv = await _context.svalue.Where(a => a.salaryId == id).ToListAsync();
            if (sv != null)
            {
                foreach (var item in sv)
                {
                    _context.svalue.Remove(item);
                    await _context.SaveChangesAsync();
                }

            }

            return RedirectToAction("List", new { ym = returnYM });
        }




    }
}