﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PayrollSys.Models;
using PayrollSys.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PayrollSys.Models.ViewModel;
using Microsoft.AspNetCore.Routing;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using SelectPdf;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace PayrollSys.Controllers
{
    public class PayslipController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PayslipController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var salary=_context.Salary.ToList();
            foreach (var item in salary)
            {
                Models.Employee ppl = _context.employees.Where(x => x.Email == item.emp).First();
                if (ppl == null)
                {
                    return RedirectToAction("Index");
                }
                item.employee = ppl;
                Models.Position post = _context.positions.Where(x => x.id == ppl.PositionID).First();
                item.positions = post;
            }
            return View("~/Views/Admin/Payslip/Index.cshtml",salary);
        }

        [HttpPost]
        public ActionResult Index(string month)
        {
            var sal = _context.Salary.Where(x => x.ym == month).ToList();
            foreach (var item in sal)
            {
                Models.Employee ppl = _context.employees.Where(x => x.Email == item.emp).First();
                if (ppl == null)
                {
                    return RedirectToAction("Index");
                }
                item.employee = ppl;
                Models.Position post = _context.positions.Where(x => x.id == ppl.PositionID).First();
                item.positions = post;
            }
            return View("~/Views/Admin/Payslip/Index.cshtml", sal);
        }

        [HttpGet]
        public ActionResult Payslip(int id)
        {
            
            Payslip sal = new Payslip();
            List<Allowance> alc = new List<Allowance>();
            List<Bonus> blc = new List<Bonus>();
            List<Deduct> dlc = new List<Deduct>();
            var salary = _context.Salary.Where(x => x.SalaryId == id).First();

            // Get value from avalue
            var a = _context.avalue.Where(x => x.salaryId == id).ToList();
            foreach (var item in a)
            {
                Allowance all = _context.Allowance.Where(x => x.AllowanceId == item.allowanceId).First();
                alc.Add(new Allowance() { AllowanceId = all.AllowanceId, criteria=all.criteria, rate=all.rate });
                
            }
            sal.allowance = alc;

            // Get value from bvalue
            var b = _context.bvalue.Where(x => x.salaryId == id).ToList();
            double brate = 0;
            foreach (var item in b)
            {
                Bonus bn = _context.Bonus.Where(x => x.BonusId == item.bonusId).First();
                brate = 0;
                brate = Math.Round(bn.rate / 100 * salary.grossSalary,2);
                blc.Add(new Bonus() { BonusId = bn.BonusId, criteria = bn.criteria, rate = brate });
                 
            }
            sal.bonus = blc;

            // Get value from dvalue
            var d = _context.dvalue.Where(x => x.salaryId == id).ToList();
            foreach (var item in d)
            {
                Deduct dec = _context.Deduct.Where(x => x.DeductId == item.deductId).First();
                
                if (item.deductId == 1)
                {
                    //calculate adsent day
                    var at = _context.attendance.Where(x => x.emp == salary.emp && x.Month == salary.ym).ToList();
                    int counter = 0;
                    foreach (var day in at)
                    {
                       if (day.Status == "Absent")
                       {
                           counter++;
                       }
                    }

                    if (counter > 0)
                    {
                        double absent = 0;
                        absent = Math.Round(salary.grossSalary / dec.rate * counter,2);
                        dlc.Add(new Deduct() { DeductId = dec.DeductId, criteria = dec.criteria, rate = absent });
                    }

                }
                else if (item.deductId == 2)
                {
                   // verify got request advance
                   var av = _context.advances.Where(x => x.employeeName == salary.emp && x.ym == salary.ym).ToList();
                   foreach (var advance in av)
                   {
                      if (advance.status == "Approved")
                      {
                         double dv = 0;
                         dv = Math.Round(salary.grossSalary * dec.rate / 100,2);
                         dlc.Add(new Deduct() { DeductId = dec.DeductId, criteria = dec.criteria, rate = dv });
                      }
                   }

                }
                else
                {
                    double dv = 0;
                    dv = Math.Round(salary.grossSalary * dec.rate / 100,2);
                    dlc.Add(new Deduct() { DeductId = dec.DeductId, criteria = dec.criteria, rate = dv });
                }
            }

            sal.deduct = dlc;

            var e = _context.evalue.Where(x => x.salaryId == id).ToList();
            var f = _context.svalue.Where(x => x.salaryId == id).ToList();
            sal.emp = salary.emp;
            sal.ym = salary.ym;
            sal.aamount = salary.aamount;
            sal.avalue = a;
            sal.bamount = salary.bamount;
            sal.bvalue = b;
            sal.damount = salary.damount;
            sal.dvalue = d;
            sal.epf = salary.epf;
            sal.socso = salary.socso;
            sal.grossSalary = salary.grossSalary;
            sal.netSalary = salary.netSalary;

            sal.earning = Math.Round(salary.aamount + salary.bamount + salary.grossSalary,2);
            sal.ded = Math.Round(salary.epf + salary.socso + salary.damount,2);

            Employee ppl = _context.employees.Where(x => x.Email == salary.emp).First();
            sal.employee = ppl;
            Position post = _context.positions.Where(x => x.id == ppl.PositionID).First();
            sal.positions = post;

            return View("~/Views/Admin/Payslip/slip.cshtml", sal);
        }

        public ActionResult Pdf(Payslip model)
        {
            var ym = model.ym;
            var id = model.employee.Id;
            var name = model.employee.fullName;
            var ic = model.employee.ICNo;
            var ph = model.employee.PhoneNo;
            var position = model.positions.job;
            var email = model.emp;
            var gs = model.grossSalary;
            List<Allowance> all = model.allowance;
            List<Bonus> bn = model.bonus;
            List<Deduct> ded = model.deduct;
            var epf = model.epf;
            var socso = model.socso;
            var earn = model.earning;
            var deduct = model.ded;
            var net = model.netSalary;

            string ca = "";
            if (all!=null)
            {
                ca = "<tr><th class='col-sm-12'>Allowance</th></tr>";
                foreach (var item in all)
                {
                    ca += "<tr><td class='col-sm-8'>"+item.criteria+"</td><td class='col-sm-4'>"+ item.rate+"</td></tr>";
                    
                }

            }

            
            var cb = "";
            if (bn!= null)
            {
                 cb = " <tr><th class='col-sm-12'>Bonus</th></tr>";
                 foreach(var item in bn)
                 {
                    
                    cb += "<tr><td class='col-sm-8'>"+item.criteria+"</td><td class='col-sm-4'>" +
                            item.rate+ "</td></tr>";
                 }
            }

            var cd = "";
            if (ded!= null)
            {
                foreach (var item in ded)
                {
                    cd += "<tr><td class='col-sm-8'>" + item.criteria + "</td>" +
                        "<td class='col-sm-4'>" + item.rate + "</td></tr>";

                }
            }


            var HtmlString = "<!DOCTYPE html><html lang='en'><head><meta charset = 'utf-8' /><meta name = 'viewport' content = 'width=device-width, initial-scale=1.0' />" +
                             "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css' integrity='sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk' crossorigin='anonymous'></ head >" +
                             "<div class='card'><div class='card-body'><center><h2>V3X Malaysia Sdn Bhd</h2><p>VISION, VIRTUAL AND VISUALIZATION</p>" +
                             "<h5>Payslip for the period of " + ym + "</h5></center><div class='row'><div class='col-sm-3'>Employee Id</div>" +
                             "<div class='col-sm-3'>: " + id + "</div><div class='col-sm-3'>Name</div>" +
                             "<div class='col-sm-3'>: " + name + "</div></div><div class='row'><div class='col-sm-3'>IC</div>" +
                             "<div class='col-sm-3'>: " + ic + "</div><div class='col-sm-3'>Contact Number</div>" +
                             "<div class='col-sm-3'>: " + ph + "</div></div><div class='row'><div class='col-sm-3'>Position</div>" +
                             "<div class='col-sm-3'>: " + position + "</div><div class='col-sm-3'>Email</div>" +
                             "<div class='col-sm-3'>: " + email + "</div></div><br /><div class='row'><div class='col-sm-6 border'><table><tr><th class='col-sm-8'>Earnings</th><th class='col-sm-4'>Amount</th></tr>" +
                             "</table></div><div class='col-sm-6 border'><table><tr><th class='col-sm-8'>Deductions</th><th class='col-sm-4'>Amount</th></tr></table></div></div>" +
                             "<div class='row'><div class='col-sm-6 border'><table><tr><td class='col-sm-8'>Basic Salary</td>" +
                             "<td class='col-sm-4'>" + gs + "</td></tr>" + ca + cb + "</table></div><div class='col-sm-6 border'><table>" + cd + "<tr>" +
                             "<th class='col-sm-12'>Government Payment</th></tr><tr><td class='col-sm-8'>EPF</td><td class='col-sm-4'>" + epf + "</td></tr>" +
                             "<tr><td class='col-sm-8'>Socso</td><td class='col-sm-4'>" + socso + "</td></tr></table></div></div> <div class='row'>" +
                             "<div class='col-sm-6 border'><table><tr><td class='col-sm-8'>Total Earnings</td><td class='col-sm-4'>" + earn + "</td>" +
                             "</tr></table></div><div class='col-sm-6 border'><table><tr><td class='col-sm-8'>Total Deductions</td><td class='col-sm-4'>" + deduct + "</td>" +
                             "</tr></table></div></div><div class='row border bg-light'><div class='col-sm-6 '></div><div class='col-sm-6 '><table>" +
                             "<tr><td class='col-sm-8'>Net Pay</td><td class='col-sm-4'>" + net + "</td></tr></table></div></div><br /><br /><div class='row'><div class='col-lg-3'>_____________________________</div>" +
                             "<div class='col-lg-6'></div><div class='col-lg-3'>_____________________________</div></div><div class='row'><div class='col-lg-3'>Employer's Signature</div>" +
                             "<div class='col-lg-6'></div><div class='col-lg-3'>Employee's Signature</div></div></div></div>";

            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Landscape;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginTop = 50;
            converter.Options.MarginBottom = 10;
            

            PdfDocument doc = converter.ConvertHtmlString(HtmlString);
            MemoryStream pdfStream = new MemoryStream();


            doc.Save(pdfStream);
            pdfStream.Position = 0;
            doc.Close();
            return File(pdfStream.ToArray(), "application/pdf");
        }

        public ActionResult Email (Payslip model)
        {
            var ym = model.ym;
            var id = model.employee.Id;
            var name = model.employee.fullName;
            var ic = model.employee.ICNo;
            var ph = model.employee.PhoneNo;
            var position = model.positions.job;
            var email = model.emp;
            var gs = model.grossSalary;
            List<Allowance> all = model.allowance;
            List<Bonus> bn = model.bonus;
            List<Deduct> ded = model.deduct;
            var epf = model.epf;
            var socso = model.socso;
            var earn = model.earning;
            var deduct = model.ded;
            var net = model.netSalary;

            string ca = "";
            if (all!= null)
            {
                ca = "<tr><th class='col-sm-12'>Allowance</th></tr>";
                foreach (var item in all)
                {
                    ca += "<tr><td class='col-sm-8'>" + item.criteria + "</td><td class='col-sm-4'>" + item.rate + "</td></tr>";

                }

            }

            var cb = "";
            if (bn != null)
            {
                cb = " <tr><th class='col-sm-12'>Bonus</th></tr>";
                foreach (var item in bn)
                {

                    cb += "<tr><td class='col-sm-8'>" + item.criteria + "</td><td class='col-sm-4'>" +
                            item.rate + "</td></tr>";
                }
            }

            var cd = "";
            if (ded != null)
            {
                foreach (var item in ded)
                {
                    cd += "<tr><td class='col-sm-8'>" + item.criteria + "</td>" +
                        "<td class='col-sm-4'>" + item.rate + "</td></tr>";

                }
            }


            var HtmlString = "<!DOCTYPE html><html lang='en'><head><meta charset = 'utf-8' /><meta name = 'viewport' content = 'width=device-width, initial-scale=1.0' />" +
                             "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css' integrity='sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk' crossorigin='anonymous'></ head >" +
                             "<div class='card'><div class='card-body'><center><h2>V3X Malaysia Sdn Bhd</h2><p>VISION, VIRTUAL AND VISUALIZATION</p>" +
                             "<h5>Payslip for the period of " + ym + "</h5></center><div class='row'><div class='col-sm-3'>Employee Id</div>" +
                             "<div class='col-sm-3'>: " + id + "</div><div class='col-sm-3'>Name</div>" +
                             "<div class='col-sm-3'>: " + name + "</div></div><div class='row'><div class='col-sm-3'>IC</div>" +
                             "<div class='col-sm-3'>: " + ic + "</div><div class='col-sm-3'>Contact Number</div>" +
                             "<div class='col-sm-3'>: " + ph + "</div></div><div class='row'><div class='col-sm-3'>Position</div>" +
                             "<div class='col-sm-3'>: " + position + "</div><div class='col-sm-3'>Email</div>" +
                             "<div class='col-sm-3'>: " + email + "</div></div><br /><div class='row'><div class='col-sm-6 border'><table><tr><th class='col-sm-8'>Earnings</th><th class='col-sm-4'>Amount</th></tr>" +
                             "</table></div><div class='col-sm-6 border'><table><tr><th class='col-sm-8'>Deductions</th><th class='col-sm-4'>Amount</th></tr></table></div></div>" +
                             "<div class='row'><div class='col-sm-6 border'><table><tr><td class='col-sm-8'>Basic Salary</td>" +
                             "<td class='col-sm-4'>" + gs + "</td></tr>" + ca + cb + "</table></div><div class='col-sm-6 border'><table>" + cd + "<tr>" +
                             "<th class='col-sm-12'>Government Payment</th></tr><tr><td class='col-sm-8'>EPF</td><td class='col-sm-4'>" + epf + "</td></tr>" +
                             "<tr><td class='col-sm-8'>Socso</td><td class='col-sm-4'>" + socso + "</td></tr></table></div></div> <div class='row'>" +
                             "<div class='col-sm-6 border'><table><tr><td class='col-sm-8'>Total Earnings</td><td class='col-sm-4'>" + earn + "</td>" +
                             "</tr></table></div><div class='col-sm-6 border'><table><tr><td class='col-sm-8'>Total Deductions</td><td class='col-sm-4'>" + deduct + "</td>" +
                             "</tr></table></div></div><div class='row border bg-light'><div class='col-sm-6 '></div><div class='col-sm-6 '><table>" +
                             "<tr><td class='col-sm-8'>Net Pay</td><td class='col-sm-4'>" + net + "</td></tr></table></div></div><br /><br /><div class='row'><div class='col-lg-3'>_____________________________</div>" +
                             "<div class='col-lg-6'></div><div class='col-lg-3'>_____________________________</div></div><div class='row'><div class='col-lg-3'>Employer's Signature</div>" +
                             "<div class='col-lg-6'></div><div class='col-lg-3'>Employee's Signature</div></div></div></div>";

            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Landscape;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginTop = 50;
            converter.Options.MarginBottom = 10;


            PdfDocument doc = converter.ConvertHtmlString(HtmlString);
            MemoryStream pdfStream = new MemoryStream();


            doc.Save(pdfStream);
            pdfStream.Position = 0;

            MailMessage message = new MailMessage("keypass59@gmail.com", email);
            message.Subject = "Payslip";
            message.Body = "The following is your payslip for " +ym;
            message.Attachments.Add(new Attachment(pdfStream, "PaySlip" + name + ym + ".pdf"));

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;

            NetworkCredential nc = new NetworkCredential("keypass59@gmail.com", "kipas95!");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = nc;

            // send email
            smtp.Send(message);

            return RedirectToAction(nameof(Index));
        }

    }
}