﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Data;
using PayrollSys.Models;

namespace PayrollSys.Controllers
{
    public class EPFController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EPFController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EPF
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/managesalary/EPF/Index.cshtml",await _context.EPF.ToListAsync());
        }

        // GET: EPF/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/managesalary/EPF/Create.cshtml");
        }

        // POST: EPF/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EPFId,Employee,Employer")] EPF ePF)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ePF);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/EPF/Create.cshtml",ePF);
        }

        // GET: EPF/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ePF = await _context.EPF.FindAsync(id);
            if (ePF == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/managesalary/EPF/Edit.cshtml",ePF);
        }

        // POST: EPF/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EPFId,Employee,Employer")] EPF ePF)
        {
            if (id != ePF.EPFId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ePF);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EPFExists(ePF.EPFId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/EPF/Edit.cshtml",ePF);
        }

        // GET: EPF/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ePF = await _context.EPF
                .FirstOrDefaultAsync(m => m.EPFId == id);
            if (ePF == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/EPF/Delete.cshtml",ePF);
        }

        // POST: EPF/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ePF = await _context.EPF.FindAsync(id);
            _context.EPF.Remove(ePF);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EPFExists(int id)
        {
            return _context.EPF.Any(e => e.EPFId == id);
        }
    }
}
