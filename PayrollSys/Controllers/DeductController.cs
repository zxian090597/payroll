﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Data;
using PayrollSys.Models;

namespace PayrollSys.Controllers
{
    public class DeductController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DeductController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Deduct
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/managesalary/Deduct/Index.cshtml",await _context.Deduct.ToListAsync());
        }

        // GET: Deduct/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deduct = await _context.Deduct
                .FirstOrDefaultAsync(m => m.DeductId == id);
            if (deduct == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Deduct/Details.cshtml",deduct);
        }

        // GET: Deduct/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/managesalary/Deduct/Create.cshtml");
        }

        // POST: Deduct/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DeductId,criteria,rate,note")] Deduct deduct)
        {
            if (ModelState.IsValid)
            {
                deduct.criteria = char.ToUpper(deduct.criteria[0]) + deduct.criteria.Substring(1);
                deduct.note = char.ToUpper(deduct.note[0]) + deduct.note.Substring(1);
                _context.Add(deduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Deduct/Create.cshtml",deduct);
        }

        // GET: Deduct/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deduct = await _context.Deduct.FindAsync(id);
            if (deduct == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/managesalary/Deduct/Edit.cshtml",deduct);
        }

        // POST: Deduct/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DeductId,criteria,rate,note")] Deduct deduct)
        {
            if (id != deduct.DeductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    deduct.criteria = char.ToUpper(deduct.criteria[0]) + deduct.criteria.Substring(1);
                    deduct.note = char.ToUpper(deduct.note[0]) + deduct.note.Substring(1);
                    _context.Update(deduct);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeductExists(deduct.DeductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Deduct/Edit.cshtml",deduct);
        }

        // GET: Deduct/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deduct = await _context.Deduct
                .FirstOrDefaultAsync(m => m.DeductId == id);
            if (deduct == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Deduct/Delete.cshtml",deduct);
        }

        // POST: Deduct/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deduct = await _context.Deduct.FindAsync(id);
            _context.Deduct.Remove(deduct);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeductExists(int id)
        {
            return _context.Deduct.Any(e => e.DeductId == id);
        }
    }
}
