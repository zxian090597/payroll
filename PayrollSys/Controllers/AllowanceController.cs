﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Data;
using PayrollSys.Models;

namespace PayrollSys.Controllers
{
    public class AllowanceController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AllowanceController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Allowance
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/managesalary/Allowance/Index.cshtml",await _context.Allowance.ToListAsync());
        }

        // GET: Allowance/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var allowance = await _context.Allowance
                .FirstOrDefaultAsync(m => m.AllowanceId == id);
            if (allowance == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Allowance/Details.cshtml",allowance);
        }

        // GET: Allowance/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/managesalary/Allowance/Create.cshtml");
        }

        // POST: Allowance/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AllowanceId,criteria,rate,note")] Allowance allowance)
        {
            if (ModelState.IsValid)
            {
                allowance.criteria = char.ToUpper(allowance.criteria[0]) + allowance.criteria.Substring(1);
                allowance.note = char.ToUpper(allowance.note[0]) + allowance.note.Substring(1);
                _context.Add(allowance);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Allowance/Create.cshtml",allowance);
        }

        // GET: Allowance/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var allowance = await _context.Allowance.FindAsync(id);
            if (allowance == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/managesalary/Allowance/Edit.cshtml",allowance);
        }

        // POST: Allowance/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AllowanceId,criteria,rate,note")] Allowance allowance)
        {
            if (id != allowance.AllowanceId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    allowance.criteria = char.ToUpper(allowance.criteria[0]) + allowance.criteria.Substring(1);
                    allowance.note = char.ToUpper(allowance.note[0]) + allowance.note.Substring(1);
                    _context.Update(allowance);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AllowanceExists(allowance.AllowanceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Allowance/Edit.cshtml",allowance);
        }

        // GET: Allowance/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var allowance = await _context.Allowance
                .FirstOrDefaultAsync(m => m.AllowanceId == id);
            if (allowance == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Allowance/Delete.cshtml",allowance);
        }

        // POST: Allowance/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var allowance = await _context.Allowance.FindAsync(id);
            _context.Allowance.Remove(allowance);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AllowanceExists(int id)
        {
            return _context.Allowance.Any(e => e.AllowanceId == id);
        }
    }
}
