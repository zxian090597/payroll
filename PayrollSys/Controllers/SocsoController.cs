﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PayrollSys.Data;
using PayrollSys.Models;

namespace PayrollSys.Controllers
{
    public class SocsoController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SocsoController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Socsoes
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/managesalary/Socso/Index.cshtml",await _context.Socso.ToListAsync());
        }

        // GET: Socsoes/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/managesalary/Socso/Create.cshtml");
        }

        // POST: Socsoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SocsoId,Pay_From,Pay_To,Employee,Employer")] Socso socso)
        {
            if (ModelState.IsValid)
            {
                _context.Add(socso);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Socso/Create.cshtml",socso);
        }

        // GET: Socsoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socso = await _context.Socso.FindAsync(id);
            if (socso == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/managesalary/Socso/Edit.cshtml",socso);
        }

        // POST: Socsoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SocsoId,Pay_From,Pay_To,Employee,Employer")] Socso socso)
        {
            if (id != socso.SocsoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(socso);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SocsoExists(socso.SocsoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/managesalary/Socso/Edit.cshtml",socso);
        }

        // GET: Socsoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socso = await _context.Socso
                .FirstOrDefaultAsync(m => m.SocsoId == id);
            if (socso == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/managesalary/Socso/Delete.cshtml",socso);
        }

        // POST: Socsoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var socso = await _context.Socso.FindAsync(id);
            _context.Socso.Remove(socso);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SocsoExists(int id)
        {
            return _context.Socso.Any(e => e.SocsoId == id);
        }
    }
}
