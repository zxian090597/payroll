﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PayrollSys.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PayrollSys.Data;

namespace PayrollSys.Controllers
{
    [Authorize(Roles ="SuperAdmin")]
    public class SuperAdminController : Controller
    {
        
        private UserManager<AppUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private IPasswordHasher<AppUser> _passwordHasher;
        private ApplicationDbContext _context;

        public SuperAdminController(UserManager<AppUser> _userMgr, RoleManager<IdentityRole> roleManager, IPasswordHasher<AppUser> passwordHash, ApplicationDbContext context)
        {
            _userManager = _userMgr;
            _roleManager = roleManager;
            _passwordHasher = passwordHash;
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ListOut()
        {
            var user = _context.admins.ToList();
            return View(user);
        }

        public ViewResult CreateAdmin() => View();

        [HttpPost]
        public async Task<IActionResult> CreateAdmin(Admin admin)
        {
            if (ModelState.IsValid)
            {
                var role = _roleManager.FindByNameAsync(admin.RoleName).Result;
                AppUser appUser = new AppUser
                {
                    UserName = admin.Email,
                    Email = admin.Email
                };

                IdentityResult result = await _userManager.CreateAsync(appUser, admin.Password);
                if (result.Succeeded)
                {
                    Admin ad = new Admin
                    {
                        fullName = admin.fullName,
                        Password = admin.Password,
                        Email = admin.Email,
                        PhoneNo = admin.PhoneNo,
                        RoleName = admin.RoleName
                    };
                    _context.Add(ad);
                    _context.SaveChanges();
                    await _userManager.AddToRoleAsync(appUser, role.Name);
                    return RedirectToAction("Listout");
                }
                else
                {
                    foreach (IdentityError error in result.Errors)
                        ModelState.AddModelError("", error.Description);
                }
            }
            return View(admin);
        }

        [HttpGet]
        public IActionResult UpdateAdmin(int id)
        {
            Admin ad = _context.admins.Where(x => x.id == id).First();
            if(ad != null)
            {
                return View(ad);
            }
            else
                ModelState.AddModelError("", "User Not Found");
            return RedirectToAction("Listout");
        }

        [HttpPost]
        public IActionResult UpdateAdmin(Admin model)
        {
            if (ModelState.IsValid)
            {
                Admin ad = _context.admins.Where(x => x.id == model.id).First();
                if (ad != null)
                {
                    ad.fullName = model.fullName;
                    ad.Password = model.Password;
                    ad.Email = model.Email;
                    ad.PhoneNo = model.PhoneNo;
                    _context.SaveChanges();
                }
                return RedirectToAction("Listout");
            }
            else
            {
                ModelState.AddModelError("", "error!");
                return View(model);
            }
        }

        private void Errors(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
                ModelState.AddModelError("", error.Description);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAdmin(string email)
        {
            AppUser user = await _userManager.FindByNameAsync(email);
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    Admin ad = _context.admins.Where(x => x.Email == email).First();
                    if(ad != null)
                    {
                        _context.Remove(ad);
                        _context.SaveChanges();
                    }
                    return RedirectToAction("Listout");
                }
                else
                    Errors(result);
            }
            else
                ModelState.AddModelError("", "User Not Found");
            return View("Index", _userManager.Users);
        }
    }
}